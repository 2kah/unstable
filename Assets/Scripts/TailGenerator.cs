using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailGenerator : MonoBehaviour
{
    public Transform TailRoot;
    public GameObject TailSectionPrefab;
    public float NumSections;

    private AddImpulseToPoint _player;

    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<AddImpulseToPoint>();

        var prevSection = TailRoot;
        for(var i = 0; i < NumSections; i++)
		{
            var section = Instantiate(TailSectionPrefab, transform);
            section.transform.position = prevSection.position - new Vector3(section.transform.localScale.x * 1.5f, 0f, 0f);
            section.GetComponent<HingeJoint2D>().connectedBody = prevSection.GetComponent<Rigidbody2D>();
            prevSection = section.transform;
		}

        prevSection.tag = "TailEnd";
        _player.TailEnd = prevSection.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
