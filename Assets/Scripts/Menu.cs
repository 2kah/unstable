using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public GameObject MenuPanel, Horse, FrustrationSlider, FlySpawner;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
	{
        Horse.SetActive(true);
        FrustrationSlider.SetActive(true);
        FlySpawner.SetActive(true);

        Time.timeScale = 1f;
        MenuPanel.SetActive(false);
	}
}
