using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fly : MonoBehaviour
{
    public float Lifetime;
    public Image TimerImage;

    private float _spawnTime;
    HorseFrustration _horseFrus;

    // Start is called before the first frame update
    void Start()
    {
        _spawnTime = Time.time;
        HorseFrustration.TotalFlies++;
        _horseFrus = FindObjectOfType<HorseFrustration>();
    }

    // Update is called once per frame
    void Update()
    {
        var timeAlive = Time.time - _spawnTime;
        if(timeAlive >= Lifetime)
		{
            Die(false);
            return;
		}
        TimerImage.fillAmount = (Lifetime - timeAlive) / Lifetime;
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
        if (collision.tag == "TailEnd")
        {
            Die(true);
        }
    }

    void Die(bool swatted)
	{
        if(swatted) { _horseFrus.OnFlySwatted(); }
        else { _horseFrus.OnFlyMissed(); }
        HorseFrustration.TotalFlies--;
        StopAllCoroutines();
        Destroy(gameObject);
	}
}
