using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlySpawner : MonoBehaviour
{
    public float minWait, maxWait;

    public GameObject FlyPrefab;
    public Transform SpawnZone;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator Spawn()
	{
        while(true)
		{
            SpawnFly();
            yield return new WaitForSeconds(Random.Range(minWait, maxWait));
		}
	}

    void SpawnFly()
	{
        var fly = Instantiate(FlyPrefab, transform);
        var spawnPos2d = Random.insideUnitCircle * (SpawnZone.lossyScale / 2f);
        var spawnPos = new Vector3(spawnPos2d.x, spawnPos2d.y, 0f);
        spawnPos += SpawnZone.position;
        fly.transform.position = spawnPos;
	}
}
