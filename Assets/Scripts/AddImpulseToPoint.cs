using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddImpulseToPoint : MonoBehaviour
{
    public Rigidbody2D TailEnd;
    public float ImpulseMagnitude;
    public float ImpulseCooldown;

    float _lastImpulse;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time - _lastImpulse >= ImpulseCooldown)
		{
            var mousePos = Input.mousePosition;
            var impulsePoint = Camera.main.ScreenToWorldPoint(mousePos);
            var impulseDir = impulsePoint - TailEnd.transform.position;
            TailEnd.AddForce(impulseDir.normalized * ImpulseMagnitude);
            _lastImpulse = Time.time;
		}
    }
}
