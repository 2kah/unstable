using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class TailGraphics : MonoBehaviour
{
    LineRenderer lr;

    // Start is called before the first frame update
    void Start()
    {
        lr = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        var children = transform.GetComponentsInChildren<Transform>();
        var points = children.Select(x => x.position).ToArray();
        lr.SetPositions(points);
    }
}
