using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class HorseFrustration : MonoBehaviour
{
    public float SwatLoss, MissGain, LandedGainPerSec, WinTime;
    public static int TotalFlies = 0;

    public Slider FrustrationSlider;
    public GameObject GameOverPanel, GameWinPanel;
    public Text ScoreText, GameWinScoreText;

    public List<AudioClip> HappySounds, SadSounds;

    AudioSource _audio;
    int FliesSwatted, FliesMissed;
    float LandedTime, GameTime;
    float _frustration = 0f;

    // Start is called before the first frame update
    void Start()
    {
        _audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        GameTime += Time.deltaTime;
        Debug.Log(GameTime);
        _frustration += TotalFlies * LandedGainPerSec * Time.deltaTime;
        if(TotalFlies > 0) { LandedTime += Time.deltaTime; }
        FrustrationSlider.value = _frustration;
        CheckEndCondition();
    }

    public void OnFlySwatted()
	{
        _frustration = Mathf.Clamp01(_frustration - SwatLoss);
        FliesSwatted++;
        if(!_audio.isPlaying) { _audio.PlayOneShot(HappySounds[Random.Range(0, HappySounds.Count)]); }
	}

    public void OnFlyMissed()
	{
        _frustration += MissGain;
        FliesMissed++;
        CheckEndCondition();
        if(!_audio.isPlaying) { _audio.PlayOneShot(SadSounds[Random.Range(0, SadSounds.Count)]); }
    }

    void CheckEndCondition()
	{
        if(GameTime >= WinTime)
		{
            GameWinScoreText.text = GetScoreText();
            GameWinPanel.SetActive(true);
            Time.timeScale = 0f;
		}
        if(_frustration >= 1f)
		{
            ScoreText.text = GetScoreText();
            GameOverPanel.SetActive(true);
            Time.timeScale = 0f;
		}
	}

    string GetScoreText()
	{
        return $"Flies swatted: {FliesSwatted}\nFlies missed: {FliesMissed}\nYou let flies stay on Callum for {LandedTime:0.###} seconds";
	}

	public void Reset()
	{
        var flies = FindObjectsOfType<Fly>();
        foreach(var fly in flies)
		{
            fly.StopAllCoroutines();
            Destroy(fly.gameObject);
		}

        TotalFlies = 0;
        _frustration = 0f;
        FliesSwatted = 0;
        FliesMissed = 0;
        LandedTime = 0f;
        GameTime = 0f;
        GameOverPanel.SetActive(false);
        GameWinPanel.SetActive(false);
        Time.timeScale = 1f;
	}
}
